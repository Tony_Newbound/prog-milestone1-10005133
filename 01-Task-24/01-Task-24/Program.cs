﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_Task_24
{
    class Program
    {
        static void Main(string[] args)
        {

            var menu = 2;
            var m = "";
            var M = "";
            var ans = "";
            var exit = "";

            do
            {
            start:
                Console.Clear();

                Console.WriteLine("Please select one of the following options");

                Console.WriteLine("********************************");
                Console.WriteLine("*           Main Menu          *");
                Console.WriteLine("*                              *");
                Console.WriteLine("*           1.food             *");
                Console.WriteLine("*           2.cloths           *");
                Console.WriteLine("*                              *");
                Console.WriteLine("*                              *");
                Console.WriteLine("********************************");
                menu = int.Parse(Console.ReadLine());

                if (menu == 1)
                {
                    Console.Clear();

                    Console.WriteLine("Here is a list of my favorite fruits");

                    Console.WriteLine("********************************");
                    Console.WriteLine("*           Fruit list         *");
                    Console.WriteLine("*                              *");
                    Console.WriteLine("*           1.Apples           *");
                    Console.WriteLine("*           2.pears            *");
                    Console.WriteLine("*           3.Oranges          *");
                    Console.WriteLine("*           4.Bananas          *");
                    Console.WriteLine("*                              *");
                    Console.WriteLine("*   Press m or M to go back    *");
                    Console.WriteLine("*                              *");
                    Console.WriteLine("********************************");
                    Console.WriteLine();
                    Console.WriteLine("Press <Enter> to end the program");
                    ans = Console.ReadLine();



                    if (ans == "m")
                    {
                        goto start;                      

                    }

                    if (ans == "M")
                    {
                        goto start;
                    }

                }

                if (menu == 2)
                {

                    Console.Clear();

                    Console.WriteLine("Here is a list of my favorite cloths");

                    Console.WriteLine("********************************");
                    Console.WriteLine("*           Cloths list        *");
                    Console.WriteLine("*                              *");
                    Console.WriteLine("*           1.Pants            *");
                    Console.WriteLine("*           2.T-Shirt          *");
                    Console.WriteLine("*           3.Boxers           *");
                    Console.WriteLine("*           4.Panties          *");
                    Console.WriteLine("*                              *");
                    Console.WriteLine("*   Press m or M to go back    *");
                    Console.WriteLine("*                              *");
                    Console.WriteLine("********************************");
                    Console.WriteLine();
                    Console.WriteLine("Press <Enter> to end the program");

                    ans = Console.ReadLine();


                    if (ans == "m")
                    {
                        goto start;                        

                    }
                        if (ans == "M")
                        {
                            goto start;
                        }
                }

                if (exit == "Enter")
                {
                    goto finish;
                }
            
            
            } while (menu == 3);

            if (menu > 2)
            {
                
                Console.WriteLine("Please enter a valid selection");
                Console.ReadLine();
                menu = 2;
                Console.Clear();
            }
        finish:
            Console.WriteLine("Goodbye thanks for using this menu");
            Console.ReadKey();
        }
    }
}
