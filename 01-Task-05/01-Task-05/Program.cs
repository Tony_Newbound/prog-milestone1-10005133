﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_Task_05
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter a time from 1-24 hours");
            var time = int.Parse(Console.ReadLine());

            if (time <= 11)
            {
                Console.WriteLine($"you have entered {time}:00 am");
             
            }
            else 
            {
                Console.WriteLine($"you have entered {(time-12)} pm");
            }
        }
    }
}
