﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_Task_21
{
    class Program
    {
        static void Main(string[] args)
        {
            var a = new Dictionary<string, int>();
            var list = new List<string>();

       
            a.Add("January",31);
            a.Add("Febuary",28);
            a.Add("March",31);
            a.Add("April",30);
            a.Add("May",31);
            a.Add("June",30);
            a.Add("July",31);
            a.Add("August",31);
            a.Add("September",30);
            a.Add("October",31);
            a.Add("November",30);
            a.Add("December",31);
        
            foreach(var i in a)
            {

                if(i.Value == 31)
                {
                    list.Add(i.Key);
                }
                
            }

            Console.WriteLine($"There are {list.Count} months");
            Console.WriteLine();
            Console.WriteLine("The months that have 31 days are");
            Console.WriteLine();
            list.ForEach(Console.WriteLine);
            Console.WriteLine();
            Console.WriteLine("Thank you come again...");

            Console.ReadKey();

        }
    }
}
