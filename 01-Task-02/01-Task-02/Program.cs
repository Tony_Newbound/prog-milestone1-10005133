﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_Task_02
{
    class Program
    {
        static void Main(string[] args)
        {
            var month = "";
            int day;

            Console.WriteLine("If you would be so kind as to tell me what month you were born :)");
            month = Console.ReadLine();

            Console.WriteLine("Now because youhave told me the month i really need the day you were born (1-31)");
            day = int.Parse(Console.ReadLine());

            Console.WriteLine($"Ok so you were born in {month} and on day {day}");
        }
    }
}
