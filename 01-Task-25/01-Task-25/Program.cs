﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_Task_25
{
    class Program
    {
        static void Main(string[] args)
        {
           Console.WriteLine("Please enter a number dont be silly");
            
           var number  = Console.ReadLine();
           var a = 0;
           bool value = int.TryParse(number, out a) ;


            if (value == true)
            {

                Console.WriteLine($"You entered {number}");
            }

            else
            {
                Console.WriteLine("please restart the program and enter a number");
            }
        }
    }
}
