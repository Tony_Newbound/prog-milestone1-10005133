﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_Task_16
{
    class Program
    {
        static void Main(string[] args)
        {
            string a = "";
                        
            Console.WriteLine("Please enter a word");
            a = Console.ReadLine();

            Console.WriteLine($"Your word was {a} this word has {(a.Length)} characters");
           
        }
            
    }
}
