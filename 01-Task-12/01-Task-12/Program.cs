﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_Task_12
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("please enter a number to see if its ODD");
            var i = int.Parse(Console.ReadLine());

            bool ans = i % 2 == 1;

            Console.WriteLine($"Your number is odd {ans}");
        }
    }
}
