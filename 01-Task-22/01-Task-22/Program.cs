﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_Task_22
{
    class Program
    {
        static void Main(string[] args)
        {
            var a = new Dictionary<string, string>();
            var list1 = new List<string>();
            var list2 = new List<string>();


            a.Add("Tomato", "Fruit");
            a.Add("Pea", "Vegtable");
            a.Add("Potato", "Vegtable");
            a.Add("Sprouts", "Vegtable");
            a.Add("Apple", "Fruit");
            a.Add("Banana", "Fruit");
            a.Add("Squash", "Vegtable");
            a.Add("carrot", "Vegtable");
            a.Add("Lemon", "Fruit");
          

            foreach (var i in a)
            {

                if (i.Value == "Fruit")
                {
                    list1.Add(i.Key);
                }
                if (i.Value == "Vegtable")
                {
                    list2.Add(i.Key);
                }

            }

            Console.WriteLine($"There are {list1.Count} Fruits and also {list2.Count} Vegtables");
            Console.WriteLine();
            Console.WriteLine("The Fruits are");
            Console.WriteLine();
            list1.ForEach(Console.WriteLine);
            Console.WriteLine();
            Console.WriteLine("Thank you come again...");

            Console.ReadKey();
        }
    }
}
