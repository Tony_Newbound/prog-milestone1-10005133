﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_Task_13
{
    class Program
    {
        static void Main(string[] args)
        {
            double num1 = 0;
            double num2 = 0;
            double num3 = 0;
            double ans = 0;
            Console.WriteLine("Please enter a dollar ammount(e.g.5.00)");
            num1 = double.Parse(Console.ReadLine());
            Console.WriteLine();

            Console.WriteLine("Please enter a another amount");
            num2 = double.Parse(Console.ReadLine());
            Console.WriteLine();


            Console.WriteLine("And one more");
            num3 = double.Parse(Console.ReadLine());
            Console.WriteLine();

            ans = num1 + num2 + num3;

            Console.WriteLine($"Your total ammount is ${(ans) * 1.15} this includes the GST");
            Console.WriteLine();


            Console.WriteLine("Now Pay me, Thankyou please come again");
            Console.WriteLine();

        }
    }
}
