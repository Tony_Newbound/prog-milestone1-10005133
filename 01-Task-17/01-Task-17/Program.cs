﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_Task_17
{
    class Program
    {
        static void Main(string[] args)
        {
            var name = new List<Tuple<string, int>>();
            name.Add(Tuple.Create("tony", 32));
            name.Add(Tuple.Create("mark", 27));        
            name.Add(Tuple.Create("jack", 20));

            foreach (var a in name)
            {
                Console.WriteLine($"{ a.Item1} is { a.Item2} years old ");
            }
        }
    }
}
