﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_Task_20
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] a = new int[7] { 33, 45, 21, 67, 87, 44, 86};
            var b = new List<int> ();
            


            foreach (int i in a)
            {
                if (i % 2 == 1)
                {
                    b.Add(i);
                }                     
            }

            Console.WriteLine($"these are {b.Count()} odd numbers");
            Console.WriteLine($"{string.Join(",", b)}");

            Console.ReadLine();
        }
           
            
        }
    }

