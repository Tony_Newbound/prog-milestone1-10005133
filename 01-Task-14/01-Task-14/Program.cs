﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_Task_14
{
    class Program
    {
        static void Main(string[] args)
        {
            var Gb = 1024;
            var Tb = 0;

            Console.WriteLine("Please enter the amount of TB you want to convert into GB?? [Example 1]");
            Tb = int.Parse(Console.ReadLine());
            Console.WriteLine();

            Console.WriteLine("One Tb is equal to {0} gigabytes.", Gb);
            Console.WriteLine();

            Console.WriteLine($"You entered {Tb} Tb this is {(Tb*1024)} Gb");
            Console.WriteLine();

            Console.WriteLine("********************** Thanks for taking part ***************************");
            Console.ReadKey();
            Console.WriteLine();

        }
    }
}
